# ETRS712-examen-2021

correction d'examen ETRS712 de 2020-2021.









# 1) Configuration de la partie traitement

## a) Fichier de deploiement kubernetes pour la partie traitement

```
      containers:
      - name:  traitment-img
        image:  registry.gitlab.com/jojo73/monsuperdrive/tiers2:1.0.0

        ports:
        - containerPort:  9000
          name:  http
```

## b) Fichier de service kubernetes pour la partie traitement

```
spec:
  type: ClusterIP
  - port: 80
    targetPort: 9000
```


## c) Fichier de deploiement, definir les resources

```
        resources:
          requests:
            cpu: "0.3"
            memory: "500Mi"
          limits:
            cpu: "0.5"
            memory: "750Mi"
```




# 2) Deploiement de la base de données


## a) Fichier secrete pour definir le username,MDP et nom de base de données
dans cette partie j'ai définir ces configuration dans un fichier de type configmap 

```
metadata:
  name: postgres-db-config
data:
  POSTGRES_DB: postgresdb
  POSTGRES_USER: admin
  POSTGRES_PASSWORD: test123
```


## b) Fichier deploiement pour Postgres

```
          envFrom:
            - configMapRef:
                name: postgres-db-config
```


# 3) augementer les nombres de pods

```
kubectl scale --replicas=8 deploy/< NOM DE DEPLOYMENT >
```
ou soit modifié le fichier 

```
replicas: 8
```



# 4) augementer les nombres de pods

# a)



# b) autoscale

```
microk8s.kubectl autoscale --min=2 --max=16 deploy/< NOM DE DEPLOYMENT > --cpu-percent=90
```

ou soit modifié le fichier 

```
maxReplicas: 16
minReplicas: 2
```
